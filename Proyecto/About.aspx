﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Proyecto.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #content {
            color:white;
        }
        #banner {
            width:75%;
            height:0%;
            margin-top:2%;
            margin-left:12.5%;
            border-radius:15px;
        }
    </style>
    <img src="banner.jpg" id="banner" /><br /><br />
    <div id="content">
        <h2>Sobre Diproma S.A.</h2>
        <h4>DIPROMA S.A. (Distribuidora de Productos Magníficos S.A.) fue fundada en el año de 1980 por el Ing. José Manuel Ruiz Juárez, con el propósito de ofrecer a la población
            guatemalteca una variedad de productos eléctricos y electrónicos de diferentes categorías a un precio asequible para toda la población, el lema de la empresa siempre ha
            sido "Precios bajos Siempre", y se ha mantenido fiel a sus creencias desde su fundación.<br /><br />Incluso dentro del ámbito social, la empresa DIPROMA S.A. se ha caracterizado
            en sobremanera por apoyar en todo lo posible a sus clientes con tal de brindar un servicio completamente seguro y de confianza a las personas, otorgando incluso algunos 
            privilegios a los clientes frecuentes para mantener una sociedad sana entre los que la forman.<br /><br />Para formar parte de esta gran familia regístrate
            <a id="ref" runat="server" href="~/Registro"><font color="white">aquí.</font></a>
        </h4><br />
        <h2>Diproma S.A. en la actualidad:</h2>
        <h4>Ante la situación que el país está experimentando debido al Covid-19 la sociedad anónima DIPROMA S.A. está comprometida con el mejoramiento con respecto al control
            acerca de las ventas de las empresas subyacentes para poder entender el comportamiento de los consumidores finales de una manera rápida y óptima, esto con el fin
            tener control y evitar el acaparamiento de productos por parte de las personas.<br />Para ello, DIPROMA S.A. ha automatizado muchas de las actividades y transferencias
            que se realizan ahora a través de una moderna aplicación web (WebApp), para que los procesos internos que involucren a cualquiera de las partes de la empresa puedan ser
            seguros, eficientes y 100% apegado a las metas asignadas.
        </h4>
    </div><br />
</asp:Content>
