﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="Proyecto.Registro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <style>
        #inicio{
            width:35%;
            height:98%;
            position:absolute;
            top:20%;
            left:32.5%;
            background-color:rgb(25,25,25);
            border:5px double white;
            color:white;
            border-radius:20px;
        }
        #titulo {
            width:100%;
            height:10%;
            color:white;
            font-size:250%;
            text-align:center;
        }
        h5 {
            width:100%;
            height:10%;
            color:white;
            font-size:120%;
            text-align:center;
            margin-top:-8%;
        }
        #box{
            width:80%;
            height:50%;
            color:white;
            font-size:120%;
            position:absolute;
            top:-3%;
            left:10%;
            background-color:green;
            color:white;
        }
        table {
            margin-top:-8%;
        }
        #b1 {
            height:10%;
            position:relative;
            background-color:rgb(25,25,25);
        }
        .active {
            width:36%;
            height:6.5%;
            background-color:rgb(25,25,25);
            border:2px solid white;
            color:white;
            position:absolute;
            top:91%;
            left:32%;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .active:hover {
            width:36%;
            height:6.5%;
            background-color:white;
            border:2px solid rgb(25,25,25);
            color:black;
            position:absolute;
            top:91%;
            left:32%;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .t {
            background-color:rgb(25,25,25);
            text-align:center;
            border:2px solid white;
            border-radius:10px;
            color:white;
            float:right;
        }
        #registro {
            width:20%;
            height:7%;
            font-size:110%;
            background-color:black;
            position:absolute;
            top:123%;
            left:40%;
            color:white;
            text-align:center;
        }
        #ref {
            text-decoration:none;
            text-decoration-color:white;
            color:white;
        }
        #foot {
            width:85.5%;
            height:9%;
            background-color:black;
            position:absolute;
            top:126%;
            left:6.5%;
        }
        .label {
            width:30%;
            height:10%;
            color:white;
            position:absolute;
            top:122%;
            left:33.5%;
            text-align:center;
            font-size:100%;
        }
    </style>
<div id="inicio">
        <h2 id="titulo">Registro de Empleados</h2><br />
        <h5>Ingrese sus datos por favor</h5>
    <center>
        <table border="0">
            <tr>
                <td>Nombres<br /></td>
                <td><div><asp:TextBox ID="nombres" CssClass="t" runat="server"></asp:TextBox></div><br /></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Apellidos</td>
                <td><div><asp:TextBox ID="apellidos" CssClass="t" runat="server"></asp:TextBox></div></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Fecha de Nacimiento &nbsp;&nbsp;</td>
                <td><div><asp:TextBox ID="nacimimeto" CssClass="t" runat="server" placeholder="DD/MM/AAAA"></asp:TextBox></div></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>NIT<br /></td>
                <td><div><asp:TextBox ID="nit" CssClass="t" runat="server"></asp:TextBox></div><br /></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Dirección</td>
                <td><div><asp:TextBox ID="direccion" CssClass="t" runat="server"></asp:TextBox></div></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Teléfono</td>
                <td><div><asp:TextBox ID="telCasa" CssClass="t" runat="server"></asp:TextBox></div></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Celular</td>
                <td><div><asp:TextBox ID="celular" CssClass="t" runat="server"></asp:TextBox></div></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>E-mail</td>
                <td><div><asp:TextBox ID="email" CssClass="t" runat="server"></asp:TextBox></div></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Contraseña</td>
                <td><div><asp:TextBox ID="contra" CssClass="t" runat="server"></asp:TextBox></div></td>
            </tr>  
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Tipo</td>
                <td><asp:RadioButton ID="gerente" runat="server" GroupName="tipo" CssClass="r" Text="Gerente" /> &nbsp;<asp:RadioButton ID="supervisor" runat="server" GroupName="tipo" CssClass="r" Text="Supervisor" /> &nbsp;<asp:RadioButton ID="vendedor" runat="server" GroupName="tipo" CssClass="r" Text="Vendedor" /></td>
            </tr>
        </table>
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Registrar" CssClass="active" />
        </center>
    </div>
    <center>
        <br /><br /><asp:Label ID="conf" CssClass="label" runat="server"></asp:Label>
    </center>
</asp:Content>
