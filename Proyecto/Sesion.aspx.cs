﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class Sesion : System.Web.UI.Page
    {
        public static String usuario;
        public static String lista;
        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden2 = "SELECT * FROM lista";
                    SqlCommand comando2 = new SqlCommand(orden2, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando2.ExecuteReader();
                    while (reg.Read())
                    {
                        DateTime inicio = reg.GetDateTime(2);
                        DateTime fin = reg.GetDateTime(3);
                        int ini = DateTime.Compare(DateTime.Today, inicio);
                        int end = DateTime.Compare(DateTime.Today, fin);
                        if ((ini >= 0) && (end <= 0))
                        {
                            Sesion.lista = reg["codigo"].ToString();
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden2 = "SELECT * FROM empleado WHERE nit='" + nit.Text + "' AND contrasena='" + contra.Text + "'";
                    SqlCommand comando2 = new SqlCommand(orden2, sqlCon);
                    comando2.Parameters.AddWithValue("nit", nit.Text);
                    sqlCon.Open();
                    SqlDataReader reg = comando2.ExecuteReader();
                    if (reg.Read())
                    {
                        String tipo = reg["tipo"].ToString();
                        Sesion.usuario = reg["nit"].ToString();
                        sqlCon.Close();
                        if (tipo.Equals("Gerente"))
                        {
                            Response.Redirect("/PerfilGerente.aspx");
                        }
                        else if (tipo.Equals("Supervisor"))
                        {
                            Response.Redirect("/PerfilSupervisor.aspx");
                        }
                        else if (tipo.Equals("Vendedor"))
                        {
                            Response.Redirect("/PerfilVendedor.aspx");
                        }
                        else if (tipo.Equals("Administrador"))
                        {
                            Response.Redirect("PerfilAdmin.aspx");
                        }
                    }
                    else
                    {
                        conf.Text = "Usuario no válido";
                    }
                }
                catch (Exception)
                {
                    conf.Text = "Error, por favor verifique e intente de nuevo";
                }
            }
        }
    }
}