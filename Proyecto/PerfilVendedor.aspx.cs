﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class PerfilVendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    if (Sesion.usuario != null)
                    {
                        setClientes();
                        try
                        {
                            SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT nit AS NIT, tipo AS Tipo, nombre AS Nombre, direccion AS Dirección, telCasa AS Teléfono, telCelular AS Celular, limiteCredito AS Crédito, diasCancelar AS Días_para_Cancelar, ordenesVencidas AS Órdenes_Vencidas FROM cliente", sqlCon);
                            DataTable dtbl = new DataTable();
                            sqlDa.Fill(dtbl);
                            tablas.DataSource = dtbl;
                            tablas.DataBind();
                            String orden = "SELECT * FROM empleado WHERE nit='" + Sesion.usuario + "'";
                            SqlCommand comando = new SqlCommand(orden, sqlCon);
                            comando.Parameters.AddWithValue("nit", Sesion.usuario);
                            sqlCon.Open();
                            SqlDataReader reg = comando.ExecuteReader();
                            if (reg.Read())
                            {
                                PerfilJefeV.jefe = reg["jefe"].ToString();
                                nombres.Text = reg["nombres"].ToString() + " " + reg["apellidos"].ToString();
                                nacimiento.Text = reg["fechaNacimiento"].ToString();
                                direccion.Text = reg["direccion"].ToString();
                                numCasa.Text = reg["numCasa"].ToString();
                                numCelular.Text = reg["numCelular"].ToString();
                                nit.Text = reg["nit"].ToString();
                                email.Text = reg["email"].ToString();
                                metaV.Text = reg["meta"].ToString();
                                monto.Text = reg["venta"].ToString();
                            }
                            sqlCon.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }

                } 
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            mv(Sesion.usuario);
        }
        public string mes(string numero)
        {
            if (numero.Equals("1"))
            {
                return "Enero";
            } else if (numero.Equals("2"))
             {
                return "Febrero";
            }
            else if (numero.Equals("3"))
            {
                return "Marzo";
            }
            else if (numero.Equals("4"))
            {
                return "Abril";
            }
            else if (numero.Equals("5"))
            {
                return "Mayo";
            }
            else if (numero.Equals("6"))
            {
                return "Junio";
            }
            else if (numero.Equals("7"))
            {
                return "Julio";
            }
            else if (numero.Equals("8"))
            {
                return "Agosto";
            }
            else if (numero.Equals("9"))
            {
                return "Septiembre";
            }
            else if (numero.Equals("10"))
            {
                return "Octubre";
            }
            else if (numero.Equals("11"))
            {
                return "Noviembre";
            }
            else if (numero.Equals("12"))
            {
                return "Diciembre";
            }
            else
            {
                return "";
            }
        }
        public float subs(string nit)
        {
            float retorno = 0;
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT COUNT(nit) AS cant FROM empleado WHERE  jefe='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("jefe", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        retorno = float.Parse(reg["cant"].ToString());
                        sqlCon.Close();
                        return retorno;
                    }
                    else
                    {
                        sqlCon.Close();
                        return retorno;
                    }
                }
                catch (Exception)
                {
                    return retorno;
                }
            }
        }
        public void agregar(Paragraph contenido, string nit)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            contenido.Add(new Paragraph("Datos del Sub-alberno:", timesN));
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit, nombres, apellidos, tipo FROM empleado WHERE nit='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - NIT: " + reg["nit"].ToString() + "\n       - Nombre: " + reg["nombres"].ToString() + " " + reg["apellidos"].ToString() + "\n       - Puesto: " + reg["tipo"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            float meta = 0;
            float pagadas = 0;
            string connectionString2 = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString2))
            {
                try
                {
                    String orden = "SELECT meta, venta FROM empleado WHERE nit='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {

                        contenido.Add(new Paragraph("Meta del mes: $" + reg["meta"].ToString() + "\n", timesN));
                        contenido.Add(new Paragraph("Monto total alcanzado: $" + reg["venta"].ToString() + "\n", timesN));
                        meta = float.Parse(reg["meta"].ToString());
                        pagadas = float.Parse(reg["venta"].ToString());
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(costoTotal) AS suma FROM orden WHERE estado='Cerrada' AND empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes cerradas del mes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(costoTotal) AS suma FROM orden WHERE estado='Pagada' AND empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes pagadas del mes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(costoTotal) AS suma FROM orden WHERE estado='Aprobada' AND empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes a pagar en meses siguientes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            float por = (pagadas / meta) * 100;
            contenido.Add(new Paragraph("Porcentaje cumplido: " + por + "% \n", timesN));
        }
        public void mv(string nit)
        {
            Document doc = new Document();
            PdfWriter arch = PdfWriter.GetInstance(doc, HttpContext.Current.Response.OutputStream);
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            doc.Open();
            Paragraph contenido = new Paragraph();
            Paragraph titulo = new Paragraph("********** DIPROMA S.A. **********\n", timesB);
            titulo.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo);
            Paragraph titulo2 = new Paragraph("Los mejores en la venta de productos electrónicos desde 2020\n\n", timesN);
            titulo2.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo2);
            Paragraph titulo3 = new Paragraph("Ventas vs Meta de empleado '" + nit + "'\n\n", timesN2);
            titulo3.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo3);
            contenido.Add(new Paragraph("Datos del Empleado:", timesN));
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit, nombres, apellidos, tipo FROM empleado WHERE nit='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - NIT: " + reg["nit"].ToString() + "\n       - Nombre: " + reg["nombres"].ToString() + " " + reg["apellidos"].ToString() + "\n       - Puesto: " + reg["tipo"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("Mes que se reporta: " + mes(DateTime.Today.Month.ToString()), timesN));
            contenido.Add(new Paragraph("Fecha y hora en que se emite el reporte: " + DateTime.Now.ToString(), timesN));
            float meta = 0;
            float pagadas = 0;
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT meta, venta FROM empleado WHERE nit='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Meta del mes: $" + reg["meta"].ToString() + "\n", timesN));
                        contenido.Add(new Paragraph("Monto total alcanzado: $" + reg["venta"].ToString() + "\n", timesN));
                        meta = float.Parse(reg["meta"].ToString());
                        pagadas = float.Parse(reg["venta"].ToString());
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(costoTotal) AS suma FROM orden WHERE estado='Cerrada' AND empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes cerradas del mes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(costoTotal) AS suma FROM orden WHERE estado='Pagada' AND empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes pagadas del mes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(costoTotal) AS suma FROM orden WHERE estado='Aprobada' AND empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes a pagar en meses siguientes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            float por = (pagadas / meta) * 100;
            contenido.Add(new Paragraph("Porcentaje cumplido: " + por + "% \n", timesN));
            if (subs(nit) > 0)
            {
                contenido.Add(new Paragraph("\nDetalle de sub-alternos: \n", timesN));
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit FROM empleado WHERE jefe='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("jefe", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    while (reg.Read())
                    {
                        contenido.Add(new Paragraph("______________________________________________________________________________________________\n", timesN));
                        agregar(contenido, reg["nit"].ToString());
                    }
                }
                catch (Exception) { }
            }
            doc.Add(contenido);
            doc.Close();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Meta_vs_Venta_" + nit + ".pdf");
            HttpContext.Current.Response.Write(doc);
            Response.Flush();
            Response.End();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            mvc(Sesion.usuario);
        }


        public void mvc(string nit)
        {
            Document doc = new Document();
            PdfWriter arch = PdfWriter.GetInstance(doc, HttpContext.Current.Response.OutputStream);
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            doc.Open();
            Paragraph contenido = new Paragraph();
            Paragraph titulo = new Paragraph("********** DIPROMA S.A. **********\n", timesB);
            titulo.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo);
            Paragraph titulo2 = new Paragraph("Los mejores en la venta de productos electrónicos desde 2020\n\n", timesN);
            titulo2.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo2);
            Paragraph titulo3 = new Paragraph("Ventas vs Meta por Categoría de empleado '" + nit + "'\n\n", timesN2);
            titulo3.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo3);
            contenido.Add(new Paragraph("Datos del Empleado:", timesN));
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit, nombres, apellidos, tipo FROM empleado WHERE nit='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - NIT: " + reg["nit"].ToString() + "\n       - Nombre: " + reg["nombres"].ToString() + " " + reg["apellidos"].ToString() + "\n       - Puesto: " + reg["tipo"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("Mes que se reporta: " + mes(DateTime.Today.Month.ToString()), timesN));
            contenido.Add(new Paragraph("Fecha y hora en que se emite el reporte: " + DateTime.Now.ToString(), timesN));
            float meta = 0;
            float pagadas = 0;
            contenido.Add(new Paragraph("\nDescripción de las categorías:\n\n", timesN));
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT categoria.nombre, metaCategoria.codigo, metaCategoria.valor FROM metaCategoria, categoria WHERE metaCategoria.codigo=categoria.codigo AND metaCategoria.empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    while (reg.Read())
                    {
                        contenido.Add(new Paragraph("Nombre: " + reg["nombre"].ToString() + ", Código: " + reg["codigo"].ToString() + "\n", timesN));
                        contenido.Add(new Paragraph("Meta del mes: $" + reg["valor"].ToString() + "\n", timesN));
                        agrCat(contenido, nit, reg["codigo"].ToString());
                        meta = float.Parse(reg["valor"].ToString());
                    }
                }
                catch (Exception) { }
            }
            if (subs(nit) > 0)
            {
                contenido.Add(new Paragraph("\nDetalle de sub-alternos: \n", timesN));
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit FROM empleado WHERE jefe='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("jefe", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    while (reg.Read())
                    {
                        contenido.Add(new Paragraph("______________________________________________________________________________________________\n", timesN));
                        agregar2(contenido, reg["nit"].ToString());
                    }
                }
                catch (Exception) { }
            }
            doc.Add(contenido);
            doc.Close();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Meta_vs_Venta_por_Categoría_" + nit + ".pdf");
            HttpContext.Current.Response.Write(doc);
            Response.Flush();
            Response.End();
        }
        public void agregar2(Paragraph contenido, string nit)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            contenido.Add(new Paragraph("Datos del Sub-alberno:", timesN));
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit, nombres, apellidos, tipo FROM empleado WHERE nit='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - NIT: " + reg["nit"].ToString() + "\n       - Nombre: " + reg["nombres"].ToString() + " " + reg["apellidos"].ToString() + "\n       - Puesto: " + reg["tipo"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            float meta = 0;
            float pagadas = 0;
            contenido.Add(new Paragraph("\nDescripción de la categoría:\n", timesN));
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT categoria.nombre, metaCategoria.codigo, metaCategoria.valor FROM metaCategoria, categoria WHERE metaCategoria.codigo=categoria.codigo AND metaCategoria.empleado='" + nit + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    while (reg.Read())
                    {
                        contenido.Add(new Paragraph("Nombre: " + reg["nombre"].ToString() + ", Código: " + reg["codigo"].ToString() + "\n", timesN));
                        contenido.Add(new Paragraph("Meta del mes: $" + reg["valor"].ToString() + "\n", timesN));
                        agrCat(contenido, nit, reg["codigo"].ToString());
                        meta = float.Parse(reg["valor"].ToString());
                    }
                }
                catch (Exception) { }
            }
        }
        public void agrCat(Paragraph contenido, string nit, string categoria)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            float meta = 0;
            float pagadas = 0;
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(carrito.subTotal) AS suma FROM carrito, orden, producto, metaCategoria, categoria WHERE (carrito.orden=orden.codigo AND carrito.nombre=producto.nombre AND producto.categoria=categoria.codigo AND producto.categoria=metaCategoria.codigo AND orden.estado='Cerrada' AND orden.empleado='" + nit + "' AND metaCategoria.codigo=" + categoria + ")";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes cerradas del mes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(carrito.subTotal) AS suma FROM carrito, orden, producto, metaCategoria, categoria WHERE (carrito.orden=orden.codigo AND carrito.nombre=producto.nombre AND producto.categoria=categoria.codigo AND producto.categoria=metaCategoria.codigo AND orden.estado='Pagada' AND orden.empleado='" + nit + "' AND metaCategoria.codigo=" + categoria + ")";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes pagadas del mes: $" + reg["suma"].ToString() + "\n", timesN));
                        pagadas = float.Parse(reg["suma"].ToString());
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(carrito.subTotal) AS suma FROM carrito, orden, producto, metaCategoria, categoria WHERE (carrito.orden=orden.codigo AND carrito.nombre=producto.nombre AND producto.categoria=categoria.codigo AND producto.categoria=metaCategoria.codigo AND orden.estado='Aprobada' AND orden.empleado='" + nit + "' AND metaCategoria.codigo=" + categoria + ")";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("empleado", nit);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("Órdenes a pagar en meses siguientes: $" + reg["suma"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            float por = (pagadas / meta) * 100;
            contenido.Add(new Paragraph("Porcentaje cumplido: " + por + "% \n\n", timesN));
        }








        //Fase 3
        public void ventasCliente(String inicio, string fin, string cCliente)
        {
            Document doc = new Document();
            PdfWriter arch = PdfWriter.GetInstance(doc, HttpContext.Current.Response.OutputStream);
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            doc.Open();
            Paragraph contenido = new Paragraph();
            Paragraph titulo = new Paragraph("********** DIPROMA S.A. **********\n", timesB);
            titulo.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo);
            Paragraph titulo2 = new Paragraph("Los mejores en la venta de productos electrónicos desde 2020\n\n", timesN);
            titulo2.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo2);
            Paragraph titulo3 = new Paragraph("Ventas por Cliente\n\n", timesN2);
            titulo3.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo3);
            contenido.Add(new Paragraph("Descripción del Cliente:", timesN));
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit, nombre, apellidos, direccion FROM cliente WHERE nit='" + cCliente + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - NIT: " + reg["nit"].ToString() + "\n       - Nombre: " + reg["nombre"].ToString() + " " + reg["apellidos"].ToString() + "\n       - Dirección: " + reg["direccion"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("Descripción del Vendedor:", timesN));
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT nit, nombres, apellidos, tipo FROM empleado WHERE nit='" + Sesion.usuario + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - NIT: " + reg["nit"].ToString() + "\n       - Nombre: " + reg["nombres"].ToString() + " " + reg["apellidos"].ToString() + "\n       - Puesto: " + reg["tipo"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("\nDetalle de venta:\n", timesN));
            contenido.Add(new Paragraph("Lista usada:", timesN));
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT codigo, nombre FROM lista WHERE codigo='" + Sesion.lista + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - Código: " + reg["codigo"].ToString() + "\n       - Nombre: " + reg["nombre"].ToString() + "\n\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT producto.codigo AS Código, producto.nombre AS Producto, AVG(precios.precio) AS Precio, SUM(carrito.cantidad) AS Cantidad, (SUM(carrito.cantidad) * AVG(precios.precio)) AS Total FROM producto, precios, carrito, orden WHERE (precios.producto=producto.codigo AND carrito.producto=producto.codigo AND carrito.orden=orden.codigo AND orden.cliente='" + cCliente + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "')) GROUP BY producto.nombre, producto.codigo";
                    SqlDataAdapter da = new SqlDataAdapter(orden, sqlCon);
                    DataTable tabla = new DataTable();
                    sqlCon.Open();
                    da.Fill(tabla);
                    PdfPTable carro = new PdfPTable(tabla.Columns.Count);
                    float[] campo = new float[tabla.Columns.Count];
                    for (int i = 0; i < tabla.Columns.Count; i++)
                    {
                        campo[i] = 4f;
                    }
                    carro.SetWidths(campo);
                    carro.WidthPercentage = 100;
                    foreach (DataColumn colu in tabla.Columns)
                    {
                        carro.AddCell(new Phrase(colu.ColumnName, timesN));
                    }
                    foreach (DataRow filass in tabla.Rows)
                    {
                        if (tabla.Rows.Count > 0)
                        {
                            for (int h = 0; h < tabla.Columns.Count; h++)
                            {
                                carro.AddCell(new Phrase(filass[h].ToString(), timesN));
                            }
                        }
                    }
                    contenido.Add(carro);
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    float total = 0;
                    String orden = "SELECT (SUM(carrito.cantidad) * AVG(precios.precio)) AS total FROM producto, precios, carrito, orden WHERE (precios.producto=producto.codigo AND carrito.producto=producto.codigo AND carrito.orden=orden.codigo AND orden.cliente='" + cCliente + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "')) GROUP BY producto.nombre, producto.codigo";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    while (reg.Read())
                    {
                        total += float.Parse(reg["total"].ToString());
                    }
                    Paragraph c = new Paragraph("Sumatoria: $" + total + "\n\n", timesN2);
                    c.Alignment = Element.ALIGN_RIGHT;
                    contenido.Add(c);
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("\nDetalle de pago:\n\n", timesN)); 
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT abono.fechaPago AS Fecha, abono.orden AS Código_Orden, abono.moneda AS Moneda_de_Pago, moneda.tasa AS Tasa_Cambio, abono.monto AS Monto_en_$ FROM abono, moneda, orden WHERE(moneda.nombre=abono.moneda AND abono.orden=orden.codigo AND orden.cliente='" + cCliente + "' AND (abono.fechaPago BETWEEN '" + inicio + "' AND '" + fin + "'))";
                    SqlDataAdapter da = new SqlDataAdapter(orden, sqlCon);
                    DataTable tabla = new DataTable();
                    sqlCon.Open();
                    da.Fill(tabla);
                    PdfPTable carro = new PdfPTable(tabla.Columns.Count);
                    float[] campo = new float[tabla.Columns.Count];
                    for (int i = 0; i < tabla.Columns.Count; i++)
                    {
                        campo[i] = 4f;
                    }
                    carro.SetWidths(campo);
                    carro.WidthPercentage = 100;
                    foreach (DataColumn colu in tabla.Columns)
                    {
                        carro.AddCell(new Phrase(colu.ColumnName, timesN));
                    }
                    foreach (DataRow filass in tabla.Rows)
                    {
                        if (tabla.Rows.Count > 0)
                        {
                            for (int h = 0; h < tabla.Columns.Count; h++)
                            {
                                carro.AddCell(new Phrase(filass[h].ToString(), timesN));
                            }
                        }
                    }
                    contenido.Add(carro);
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(abono.monto) AS suma FROM abono, moneda, orden WHERE(moneda.nombre=abono.moneda AND abono.orden=orden.codigo AND orden.cliente='" + cCliente + "' AND (abono.fechaPago BETWEEN '" + inicio + "' AND '" + fin + "'))";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        Paragraph c = new Paragraph("Sumatoria: $" + reg["suma"].ToString() + "\n\n", timesN2);
                        c.Alignment = Element.ALIGN_RIGHT;
                        contenido.Add(c);
                    }
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("\nDetalle de notas de crédito por anulación:\n\n", timesN));
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT factura.fecha AS Fecha, factura.orden AS Código_Orden, factura.monto AS Monto_en_$ FROM factura, orden WHERE (orden.codigo=factura.orden AND (factura.codigo LIKE 'FacturaAnulacion_%') AND orden.cliente='" + cCliente + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "'))";
                    SqlDataAdapter da = new SqlDataAdapter(orden, sqlCon);
                    DataTable tabla = new DataTable();
                    sqlCon.Open();
                    da.Fill(tabla);
                    PdfPTable carro = new PdfPTable(tabla.Columns.Count);
                    float[] campo = new float[tabla.Columns.Count];
                    for (int i = 0; i < tabla.Columns.Count; i++)
                    {
                        campo[i] = 4f;
                    }
                    carro.SetWidths(campo);
                    carro.WidthPercentage = 100;
                    foreach (DataColumn colu in tabla.Columns)
                    {
                        carro.AddCell(new Phrase(colu.ColumnName, timesN));
                    }
                    foreach (DataRow filass in tabla.Rows)
                    {
                        if (tabla.Rows.Count > 0)
                        {
                            for (int h = 0; h < tabla.Columns.Count; h++)
                            {
                                carro.AddCell(new Phrase(filass[h].ToString(), timesN));
                            }
                        }
                    }
                    contenido.Add(carro);
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT SUM(factura.monto) AS suma FROM factura, orden WHERE (orden.codigo=factura.orden AND (factura.codigo LIKE 'FacturaAnulacion_%') AND orden.cliente='" + cCliente + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "'))";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        Paragraph c = new Paragraph("Sumatoria: $" + reg["suma"].ToString() + "\n\n", timesN2);
                        c.Alignment = Element.ALIGN_RIGHT;
                        contenido.Add(c);
                    }
                }
                catch (Exception) { }
            }
            doc.Add(contenido);
            doc.Close();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=VentasxCliente_" + cCliente +  ".pdf");
            HttpContext.Current.Response.Write(doc);
            Response.Flush();
            Response.End();
        }
        public void ventaProducto(String inicio, string fin, string cProd)
        {
            Document doc = new Document();
            PdfWriter arch = PdfWriter.GetInstance(doc, HttpContext.Current.Response.OutputStream);
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            doc.Open();
            Paragraph contenido = new Paragraph();
            Paragraph titulo = new Paragraph("********** DIPROMA S.A. **********\n", timesB);
            titulo.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo);
            Paragraph titulo2 = new Paragraph("Los mejores en la venta de productos electrónicos desde 2020\n\n", timesN);
            titulo2.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo2);
            Paragraph titulo3 = new Paragraph("Ventas por Producto\n\n", timesN2);
            titulo3.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo3);
            contenido.Add(new Paragraph("Descripción del Producto:", timesN));
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT codigo, nombre, descripcion FROM producto WHERE codigo='" + cProd + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - Código: " + reg["codigo"].ToString() + "\n       - Nombre: " + reg["nombre"].ToString() + "\n       - Descripción: " + reg["descripcion"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("\nDetalle de ventas:\n\n", timesN));
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT orden.fcierre AS Fecha, orden.codigo AS Código_Orden, carrito.cantidad AS Cantidad, carrito.subTotal AS Precio_Total FROM orden, carrito WHERE (carrito.orden=orden.codigo AND carrito.producto='" + cProd + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "'))";
                    SqlDataAdapter da = new SqlDataAdapter(orden, sqlCon);
                    DataTable tabla = new DataTable();
                    sqlCon.Open();
                    da.Fill(tabla);
                    PdfPTable carro = new PdfPTable(tabla.Columns.Count);
                    float[] campo = new float[tabla.Columns.Count];
                    for (int i = 0; i < tabla.Columns.Count; i++)
                    {
                        campo[i] = 4f;
                    }
                    carro.SetWidths(campo);
                    carro.WidthPercentage = 100;
                    foreach (DataColumn colu in tabla.Columns)
                    {
                        carro.AddCell(new Phrase(colu.ColumnName, timesN));
                    }
                    foreach (DataRow filass in tabla.Rows)
                    {
                        if (tabla.Rows.Count > 0)
                        {
                            for (int h = 0; h < tabla.Columns.Count; h++)
                            {
                                carro.AddCell(new Phrase(filass[h].ToString(), timesN));
                            }
                        }
                    }
                    contenido.Add(carro);
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    float cant = 0;
                    float total = 0;
                    String orden = "SELECT carrito.cantidad AS cant, carrito.subTotal AS total FROM orden, carrito WHERE (carrito.orden=orden.codigo AND carrito.producto='" + cProd + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "'))";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    while (reg.Read())
                    {
                        cant += float.Parse(reg["cant"].ToString());
                        total += float.Parse(reg["total"].ToString());
                    }
                    Paragraph c = new Paragraph("Sumatoria: " + cant + "                     Sumatoria: $" + total + "\n\n", timesN2);
                    c.Alignment = Element.ALIGN_RIGHT;
                    contenido.Add(c);
                }
                catch (Exception) { }
            }
            doc.Add(contenido);
            doc.Close();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=VentasxProducto_" + cProd + ".pdf");
            HttpContext.Current.Response.Write(doc);
            Response.Flush();
            Response.End();
        }
        public void setClientes()
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT nit, nombre FROM cliente", sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    clientes.DataSource = dtbl;
                    clientes.DataTextField = "nombre";
                    clientes.DataValueField = "nit";
                    clientes.DataBind();
                    sqlCon.Close();
                }
                catch (Exception)
                {
                }

            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT codigo, nombre FROM categoria", sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    categorias.DataSource = dtbl;
                    categorias.DataTextField = "nombre";
                    categorias.DataValueField = "codigo";
                    categorias.DataBind();
                    sqlCon.Close();
                }
                catch (Exception)
                {
                }

            }
        }
        public void ventaCategoria(String inicio, string fin, string cCate)
        {
            Document doc = new Document();
            PdfWriter arch = PdfWriter.GetInstance(doc, HttpContext.Current.Response.OutputStream);
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font timesB = new Font(bf, 16, 1, BaseColor.BLACK);
            Font timesN = new Font(bf, 11, 0, BaseColor.BLACK);
            Font timesN2 = new Font(bf, 13, 0, BaseColor.BLACK);
            doc.Open();
            Paragraph contenido = new Paragraph();
            Paragraph titulo = new Paragraph("********** DIPROMA S.A. **********\n", timesB);
            titulo.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo);
            Paragraph titulo2 = new Paragraph("Los mejores en la venta de productos electrónicos desde 2020\n\n", timesN);
            titulo2.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo2);
            Paragraph titulo3 = new Paragraph("Ventas por Categoría\n\n", timesN2);
            titulo3.Alignment = Element.ALIGN_CENTER;
            contenido.Add(titulo3);
            contenido.Add(new Paragraph("Descripción de la Categoría:", timesN));
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT codigo, nombre FROM categoria WHERE codigo='" + cCate + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        contenido.Add(new Paragraph("       - Código: " + reg["codigo"].ToString() + "\n       - Nombre: " + reg["nombre"].ToString() + "\n", timesN));
                    }
                }
                catch (Exception) { }
            }
            contenido.Add(new Paragraph("\nDetalle de ventas:\n\n", timesN));
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT producto.codigo AS Código, producto.nombre AS Producto, SUM(carrito.cantidad) AS Cantidad_Total, COUNT(orden.codigo) AS Cantidad_Órdenes, AVG(orden.costoTotal) AS Promedio_Total_x_Orden, AVG(carrito.subTotal) AS Promedio_x_Orden FROM producto, carrito, orden WHERE (carrito.orden=orden.codigo AND carrito.producto=producto.codigo AND producto.categoria='" + cCate + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "')) GROUP BY producto.codigo, producto.nombre";
                    SqlDataAdapter da = new SqlDataAdapter(orden, sqlCon);
                    DataTable tabla = new DataTable();
                    sqlCon.Open();
                    da.Fill(tabla);
                    PdfPTable carro = new PdfPTable(tabla.Columns.Count);
                    float[] campo = new float[tabla.Columns.Count];
                    for (int i = 0; i < tabla.Columns.Count; i++)
                    {
                        campo[i] = 4f;
                    }
                    carro.SetWidths(campo);
                    carro.WidthPercentage = 100;
                    foreach (DataColumn colu in tabla.Columns)
                    {
                        carro.AddCell(new Phrase(colu.ColumnName, timesN));
                    }
                    foreach (DataRow filass in tabla.Rows)
                    {
                        if (tabla.Rows.Count > 0)
                        {
                            for (int h = 0; h < tabla.Columns.Count; h++)
                            {
                                carro.AddCell(new Phrase(filass[h].ToString(), timesN));
                            }
                        }
                    }
                    contenido.Add(carro);
                }
                catch (Exception) { }
            }
            connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    float tot = 0;
                    float prom = 0;
                    String orden = "SELECT AVG(orden.costoTotal) AS tot, AVG(carrito.subTotal) AS prom FROM producto, carrito, orden WHERE (carrito.orden=orden.codigo AND carrito.producto=producto.codigo AND producto.categoria='" + cCate + "' AND (orden.fcierre BETWEEN '" + inicio + "' AND '" + fin + "')) GROUP BY producto.codigo, producto.nombre";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    while (reg.Read())
                    {
                        tot += float.Parse(reg["tot"].ToString());
                        prom += float.Parse(reg["prom"].ToString());
                    }
                    Paragraph c = new Paragraph("Sumatoria Promedio: $" + tot + "          Sumatoria Promedio: $" + prom + "\n\n", timesN2);
                    c.Alignment = Element.ALIGN_RIGHT;
                    contenido.Add(c);
                }
                catch (Exception) { }
            }
            doc.Add(contenido);
            doc.Close();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=VentasxCategoría_" + cCate + ".pdf");
            HttpContext.Current.Response.Write(doc);
            Response.Flush();
            Response.End();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string[] ini = fInicio.Text.Split('/');
            string inicio = ini[2] + "/" + ini[1] + "/" + ini[0];
            string[] fi = fFin.Text.Split('/');
            string fin = fi[2] + "/" + fi[1] + "/" + fi[0];
            ventasCliente(inicio, fin, clientes.SelectedValue.ToString());
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            string[] ini = fInicio.Text.Split('/');
            string inicio = ini[2] + "/" + ini[1] + "/" + ini[0];
            string[] fi = fFin.Text.Split('/');
            string fin = fi[2] + "/" + fi[1] + "/" + fi[0];
            ventaCategoria(inicio, fin, categorias.SelectedValue.ToString());
        }

        protected void categorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT codigo, nombre FROM producto WHERE categoria='" + categorias.SelectedValue.ToString() + "'", sqlCon);
                    DataTable dtbl = new DataTable();
                    sqlDa.Fill(dtbl);
                    productos.DataSource = dtbl;
                    productos.DataTextField = "nombre";
                    productos.DataValueField = "codigo";
                    productos.DataBind();
                    productos.Enabled = true;
                    sqlCon.Close();
                }
                catch (Exception)
                {
                }

            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            string[] ini = fInicio.Text.Split('/');
            string inicio = ini[2] + "/" + ini[1] + "/" + ini[0];
            string[] fi = fFin.Text.Split('/');
            string fin = fi[2] + "/" + fi[1] + "/" + fi[0];
            ventaProducto(inicio, fin, productos.SelectedValue.ToString());
        }

    }
}