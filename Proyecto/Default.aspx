﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Proyecto._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #banner{
            background-color:rgb(25,25,25);
            border:10px double white;
            color:white;
            border-radius:20px;
        }
        #unirme {
            background-color:rgb(25,25,25);
            border:2px solid white;
            color:white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #unirme:hover {
            background-color:white;
            border:2px solid rgb(25,25,25);
            color:black;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #a1 {
            background-color:black;
            border:2px solid white;
            color:white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #a1:hover {
            background-color:white;
            border:2px solid black;
            color:black;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #a2 {
            background-color:black;
            border:2px solid white;
            color:white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #a2:hover {
            background-color:white;
            border:2px solid black;
            color:black;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #a3 {
            background-color:black;
            border:2px solid white;
            color:white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #a3:hover {
            background-color:white;
            border:2px solid black;
            color:black;
            border-radius:10px;
            transition-duration:0.3s;
        }
        #it1 {
            color:white;
        }
        #it2 {
            color:white;
        }
        #it3 {
            color:white;
        }
    </style>
    <div class="jumbotron" id="banner">
        <h1>DIPROMA S.A.&nbsp;</h1>
        <p class="lead">Forma parte de nuestra extensa red de clientes en todo el país</p>
        <p><a id="unirme" href="Registro.aspx" class="btn btn-primary btn-lg">Unirme &gt;&gt;</a></p>
    </div>

    <div class="row">
        <div id="it1" class="col-md-4">
            <h2>Beneficios</h2>
            <p>
                DIPROMA S.A. posee un basto control con respecto a los clientes y las 
                transacciones realizadas de cada uno de ellos para poder detectar el comportamiento de estos para una mayor optimización frente a escasez. Contáctanos para más información.<p>
                <a id="a1" class="btn btn-default" href="Contact.aspx">Contacto &gt;&gt;</a>
            </p>
        </div>
        <div id="it2" class="col-md-4">
            <h2>Conócenos</h2>
            <p>
                La historia que posee DIPROMA S.A. frente a las crisis socioeconómicas la convierte en una distribuidora de alto prestigio comprometida con sus clientes y distribuidores asociados, haciendo de la automatización de sistemas una experiencia más agradable al usuario</p>
            <p>
                <a id="a2" class="btn btn-default" href="About.aspx">Historia &gt;&gt;</a>
            </p>
        </div>
        <div id="it3" class="col-md-4">
            <h2>Regístrate</h2>
            <p>
                Si aún no eres parte de esta gran familia, acude a nuestras distribuidoras asociadas repartidas en todo el país, o bien regístrate en nuestra amplia red de clientes DIPROMA. Para obtener más información, contáctanos en la pestaña contacto.</p>
            <p>
                <a id="a3" class="btn btn-default" href="Registro.aspx">Regístrate &gt;&gt;</a>
            </p>
        </div>
    </div>
</asp:Content>
