﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Proyecto.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #content {
            height:120%;
            width:90%;
            background-color:black;
            color:white;
        }
    </style>
    <div id="content">
        <h2><%: Title %></h2><br />
        <h3>Nuestras ubicaciones:</h3>
        <address>
            <h4>Zona 9:</h4>
            Dirección: 7a. Avenida 12-72, Zona 9<br />
            Horario: Lunes a sábado de 09:00 a 20:00 - Domingo de 10:00 a 19:00<br />
            Teléfonos: 2339-0481 / 0410 / 0299<br />
            Email: <a href="mailto:agenciazona9@diproma.com.gt"><font color="white">agenciazona9@diproma.com.gt</font></a>
        </address>
        <address>
            <h4>Majadas:</h4>
            Dirección: 27 Avenida 5-90, Zona 11 Interior Parque Comercial Las Majadas<br />
            Horario: Lunes a sábado de 09:00 a 21:00 - Domingo de 10:00 a 20:00<br />
            Tel: 2423-1600<br />
            Email: <a href="mailto:agenciamajadas@diproma.com.gt"><font color="white">agenciamajadas@diproma.com.gt</font></a>
        </address>
        <address>
            <h4>San Cristobal:</h4>
            Dirección: Boulevard Principal, Manzana CC, Sector B-1, zona 8 de Mixco, Centro Comercial Plaza San Cristobal, Local 149<br />
            Horario: Lunes a sábado de 09:00 a 21:00 - Domingo de 10:00 a 20:00<br />
            Tels: 2443-0876 / 0990 / 0859 / 0492<br />
            Email: <a href="mailto:agenciasancris@diproma.com.gt"><font color="white">agenciasancris@diproma.com.gt</font></a>
        </address>
        <address>
            <h4>Metrocentro:</h4>
            Direccion: 0 Calle 16-20, Zona 4 de Villa Nueva, Centro Comercial Metro Centro, Local 202<br />
            Horario: Lunes a sábado de 09:00 a 21:00 - Domingo de 10:00 a 20:00<br />
            Tels: 6631-7030 / 5509/6635-7197 / 6636-8097<br />
            Email: <a href="mailto:agenciametrocentro@diproma.com.gt"><font color="white">agenciametrocentro@diproma.com.gt</font></a>
        </address>
        <address>
            <h4>Miraflores:</h4>
            Direccion: 21 Avenida 4-32, Zona 11, Centro Comercial Galerías Miraflores, 1er. Nivel Local 185 al 187<br />
            Horario: Lunes a sábado de 09:00 a 21:00 - Domingo de 10:00 a 20:00<br />
            Tels: 2474-8318 - 2474-1689/1821/1726<br />
            Email: <a href="mailto:agenciamiraflores@diproma.com.gt"><font color="white">agenciamiraflores@diproma.com.gt</font></a>
        </address><br />
        <address> 
            <h3>Atención al Cliente:</h3>
            Soporte: <a href="mailto:soporte@diproma.com.gt"><font color="white">soporte@diproma.com.gt</font></a><br />
            Servicio al Cliente: <a href="mailto:scliente@diproma.com.gt"><font color="white">scliente@diproma.com.gt</font></a>
        </address>
        <br />
    </div>
</asp:Content>
