﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class PerfilJefeV : System.Web.UI.Page
    {
        public static String jefe;
        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {

                try
                {
                    String orden = "SELECT * FROM empleado WHERE nit='" + PerfilJefeV.jefe + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("nit", Sesion.usuario);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        nombre.Text = reg["nombres"].ToString() + " " + reg["apellidos"].ToString();
                        nacimiento.Text = reg["fechaNacimiento"].ToString();
                        numCelular.Text = reg["numCelular"].ToString();
                        nit.Text = reg["nit"].ToString();
                        email.Text = reg["email"].ToString();
                        
                    }
                    
                    sqlCon.Close();
                }
                catch (Exception)
                {
                }

            }
        }
    }
}