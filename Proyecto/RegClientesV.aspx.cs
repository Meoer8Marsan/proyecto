﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class RegClientesV : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                if (individual.Checked == true)
                {
                    try
                    {
                        sqlCon.Open();
                        String[] fecha = fechaOrigen.Text.Split('/');
                        String nueva = fecha[2] + "/" + fecha[1] + "/" + fecha[0];
                        String orden = "INSERT INTO cliente (limiteCredito, diasCancelar, ordenesVencidas, tipo, nombre, apellidos, fechaOrigen, direccion, telCasa, telCelular, nit, email, depto, ciudad) VALUES (7000, 30, 0, 'Individual', '" + nombre.Text + "' , '" + apellidos.Text + "' , '" + nueva + "' , '" + direccion.Text + "' , '" + telCasa.Text + "' , '" + telCelular.Text + "' , '" + nit.Text + "' , '" + email.Text + "' , '" + depto.Text + "' , '" + municipio.Text + "')";
                        SqlDataAdapter sqlDa = new SqlDataAdapter(orden, sqlCon);
                        DataTable dtbl = new DataTable();
                        sqlDa.Fill(dtbl);
                        sqlCon.Close();
                        conf.Text = "El registro del cliente fue completado con éxito";
                    }
                    catch (Exception)
                    {
                        conf.Text = "Error al ingresar, por favor revise sus datos e intente de nuevo";
                    }
                }
                else if (empresarial.Checked == true)
                {
                    try
                    {
                        sqlCon.Open();
                        String[] fecha = fechaOrigen.Text.Split('/');
                        String nueva = fecha[2] + "/" + fecha[1] + "/" + fecha[0];
                        String orden = "INSERT INTO cliente (limiteCredito, diasCancelar, ordenesVencidas, tipo, nombre, apellidos, fechaOrigen, direccion, telCasa, telCelular, nit, email, depto, ciudad) VALUES (7000, 30, 0, 'Empresarial', '" + nombre.Text + "' , '" + apellidos.Text + "' , '" + nueva + "' , '" + direccion.Text + "' , '" + telCasa.Text + "' , '" + telCelular.Text + "' , '" + nit.Text + "' , '" + email.Text + "' , '" + depto.Text + "' , '" + municipio.Text + "')";
                        SqlDataAdapter sqlDa = new SqlDataAdapter(orden, sqlCon);
                        DataTable dtbl = new DataTable();
                        sqlDa.Fill(dtbl);
                        sqlCon.Close();
                        conf.Text = "El registro del cliente fue completado con éxito";
                    }
                    catch (Exception)
                    {
                        conf.Text = "Error al ingresar, por favor revise sus datos e intente de nuevo";
                    }
                }
            }
        }
    }
}