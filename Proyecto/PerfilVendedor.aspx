﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/PerfilV.Master" AutoEventWireup="true" CodeBehind="PerfilVendedor.aspx.cs" Inherits="Proyecto.PerfilVendedor" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Perfiles" runat="server">
    <style>
        article {
            height:86%;
            width:77.5%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:7%;
            left:4%;
            overflow:auto;
        }
        #contenido {
            height:90%;
            width:94%;
            font-size:130%;
            position:relative;
            top:2%;
            left:3%;
            background-color:black;
        }
        #meta {
            height:8.3%;
            width:36%;
            text-align:center;
            font-size:120%;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:-0.3%;
            left:63.6%;
            background-color:black;
        }
        .table {
            width:100%;
            color:white;
            background-color:black;
            border-radius:10px;
            font-family:"Times New Roman", Arial;
            font-size:70%;
            text-align:center;
        }
        .rep1{
            height:7%;
            width:20%;
            position:absolute;
            top:12%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .rep1:hover {
            height:7%;
            width:20%;
            position:absolute;
            top:12%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:black;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .rep2{
            height:7%;
            width:20%;
            position:absolute;
            top:22%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .rep2:hover {
            height:7%;
            width:20%;
            position:absolute;
            top:22%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:black;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .rep3{
            height:7%;
            width:20%;
            position:absolute;
            top:32%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .rep4{
            height:7%;
            width:20%;
            position:absolute;
            top:42%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .rep4:hover {
            height:7%;
            width:20%;
            position:absolute;
            top:42%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:black;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .r5{
            height:5%;
            width:19.1%;
            position:absolute;
            top:42%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            text-align:center;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
        }
        .r6{
            height:5%;
            width:19.1%;
            position:absolute;
            top:51%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            text-align:center;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
        }
        .r7{
            height:5%;
            width:19.1%;
            position:absolute;
            top:60%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            text-align:center;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .r7:hover {
            height:5%;
            width:19.1%;
            position:absolute;
            top:60%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:black;
            text-align:center;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .r8{
            height:5%;
            width:19.1%;
            position:absolute;
            top:68%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            text-align:center;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
        }
        .r9{
            height:5%;
            width:19.1%;
            position:absolute;
            top:76%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            text-align:center;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .r9:hover {
            height:5%;
            width:19.1%;
            position:absolute;
            top:76%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:black;
            text-align:center;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .r10{
            height:5%;
            width:19.1%;
            position:absolute;
            top:84%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            text-align:center;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
        }
        .r11{
            height:5%;
            width:19.1%;
            position:absolute;
            top:92%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:white;
            text-align:center;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .r11:hover {
            height:5%;
            width:19.1%;
            position:absolute;
            top:92%;
            left:80%;
            font-size:100%;
            font-family:"Times New Roman", Arial;
            color:black;
            text-align:center;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            transition-duration:0.3s;
        }
    </style>
        <article>
            <div id="contenido">
                <h2>Datos Personales</h2>
                Nombre: <asp:Label ID="nombres" CssClass="label" runat="server"></asp:Label><p></p>
                Fecha de Nacimiento: <asp:Label ID="nacimiento" CssClass="label" runat="server"></asp:Label><p></p>
                Dirección: <asp:Label ID="direccion" CssClass="label" runat="server"></asp:Label><p></p>
                Número de Casa: <asp:Label ID="numCasa" CssClass="label" runat="server"></asp:Label><p></p>
                Número de Celular: <asp:Label ID="numCelular" CssClass="label" runat="server"></asp:Label><br /><br />
                <h2>Datos de Empleado</h2>
                NIT: <asp:Label ID="nit" CssClass="label" runat="server"></asp:Label><p></p>
                Correo electrónico: <asp:Label ID="email" CssClass="label" runat="server"></asp:Label><p></p>
                Clientes: <br /><br />
                <asp:GridView ID="tablas" runat="server" AutoGenerateColumns="false" CssClass="table"></asp:GridView>
                <br /><br />
            </div>
            <div id="meta">
                Meta a alcanzar: <asp:Label ID="metaV" CssClass="label" runat="server"></asp:Label><br />
                Cantidad actual: &nbsp;<asp:Label ID="monto" CssClass="label" runat="server"></asp:Label>
            </div>
            <asp:Button ID="Button1" runat="server" Text="Venta vs Meta" CssClass="rep1" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="Venta vs Meta x Categoría" CssClass="rep2" OnClick="Button2_Click" />
            <asp:Button ID="Button3" runat="server" Text="Ventas por Cliente" CssClass="r7" OnClick="Button3_Click" />
            <asp:TextBox ID="fInicio" runat="server" placeholder="Fecha de Inicio" CssClass="r5"></asp:TextBox>
            <asp:TextBox ID="fFin" runat="server" placeholder="Fecha de Fin" CssClass="r6"></asp:TextBox>
            <asp:DropDownList ID="clientes" runat="server" CssClass="rep3"></asp:DropDownList>
            <asp:DropDownList ID="categorias" runat="server" CssClass="r8" OnSelectedIndexChanged="categorias_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            <asp:Button ID="Button4" runat="server" Text="Ventas por Categoría" CssClass="r9" OnClick="Button4_Click" />
            <asp:DropDownList ID="productos" runat="server" CssClass="r10" Enabled="false"></asp:DropDownList>
            <asp:Button ID="Button5" runat="server" Text="Ventas por Producto" CssClass="r11" OnClick="Button5_Click" />
        </article>
</asp:Content>
