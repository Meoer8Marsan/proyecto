﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/PerfilV.Master" AutoEventWireup="true" CodeBehind="RegClientesV.aspx.cs" Inherits="Proyecto.RegClientesV" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Perfiles" runat="server">
    <style>
        article {
            height:86%;
            width:77.5%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:7%;
            left:4%;
            overflow:auto;
        }
        #contenido {
            height:95%;
            width:94%;
            font-size:130%;
            position:relative;
            top:2%;
            left:3%;
            background-color:black;
        }
        .t1 {
            height:100%;
            width:100%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            color:white;
            font-family:"Times New Roman", Arial;
            font-size:90%;
            text-align:center;
        }
        .boton {
            height:10%;
            width:15%;
            color:white;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:132%;
            left:42.5%;
            transition-duration:0.3s;
        }
        .boton:hover {
            height:10%;
            width:15%;
            color:black;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:132%;
            left:42.5%;
            transition-duration:0.3s;
        }
        h1 {
            margin-top:-2%;
        }
    </style>
        <article>
            <div id="contenido">
                <center><br />
                <h1>Registro de Cliente Nuevo</h1><br />
                <table border="0">
                    <tr>
                        <td>Tipo de Cliente <br />&nbsp;</td><td><asp:RadioButton ID="individual" runat="server" GroupName="tipo" CssClass="r" Text="Individual" /> &nbsp;<asp:RadioButton ID="empresarial" runat="server" GroupName="tipo" CssClass="r" Text="Empresarial" /><br />&nbsp;</td>
                    </tr><tr>
                        <td>Nombre(s) &nbsp;&nbsp;<br />&nbsp;</td><td><asp:TextBox ID="nombre" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Apellidos (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="apellidos" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Fecha de Origen <br />&nbsp;</td><td><asp:TextBox ID="fechaOrigen" CssClass="t1" runat="server" placeholder="DD/MM/AAAA"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Dirección (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="direccion" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Teléfono de Casa (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="telCasa" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Teléfono Celular (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="telCelular" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>NIT (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="nit" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>E-mail (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="email" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Departamento (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="depto" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Municipio (Opcional) <br />&nbsp;</td><td><asp:TextBox ID="municipio" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr>
                </table>
                    <asp:Button ID="Button1" runat="server" CssClass="boton" Text="Registrar" OnClick="Button1_Click" /><br /><br /><br /><br />
                    <asp:Label ID="conf" CssClass="label" runat="server"></asp:Label><br /><br /><br />
                </center>
            </div>
        </article>
</asp:Content>
