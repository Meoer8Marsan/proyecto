﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/PerfilV.Master" AutoEventWireup="true" CodeBehind="AdminClientesV.aspx.cs" Inherits="Proyecto.AdminClientesV" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Perfiles" runat="server">
     <style>
        article {
            height:70%;
            width:77.5%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:22%;
            left:4%;
        }
        .busqueda {
            height:7%;
            width:65%;
            color:white;
            text-align:center;
            font-size:170%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:7.5%;
            left:4%;
        }
        .boton {
            height:8%;
            width:9%;
            color:white;
            font-size:180%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:7.5%;
            left:72.7%;
            transition-duration:0.3s;
        }
        .boton:hover {
            height:8%;
            width:9%;
            color:black;
            font-size:180%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:7.5%;
            left:72.7%;
            transition-duration:0.3s;
        }
        #contenido {
            height:90%;
            width:94%;
            font-size:130%;
            position:relative;
            top:1%;
            left:3%;
            background-color:black;
        }
        #meta {
            height:8.3%;
            width:36%;
            text-align:center;
            font-size:120%;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:-0.3%;
            left:63.8%;
            background-color:black;
        }
    </style>
        <asp:TextBox ID="TextBox1" CssClass="busqueda" runat="server" placeholder="Ingrese Código del Cliente a Revisar"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" CssClass="boton" Text="Buscar" OnClick="Button1_Click" />
        <article>
            <div id="contenido">
                <h2>Datos Generales</h2>
                Nombre: (Nombre completo del Cliente)<p></p>
                Fecha de Origen: (Fecha de nacimiento del Cliente)<p></p>
                Número de Celular: (Celular del Cliente [si tiene])<br /><br />
                <h2>Datos de Cliente</h2>
                Código: (Código de Cliente)<p></p>
                NIT: (NIT del Cliente [si tiene])<p></p>
                Correo electrónico: (E-mail del Cliente [si tiene])<p></p>
            </div>
        </article>
</asp:Content>
