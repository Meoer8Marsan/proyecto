﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class OrdenPagoV : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    try
                    {
                        SqlCommand comando = new SqlCommand("SELECT * FROM cliente WHERE ordenesVencidas=0 ORDER BY (nombre)", sqlCon);
                        SqlDataAdapter db = new SqlDataAdapter(comando);
                        DataTable ds = new DataTable();
                        db.Fill(ds);
                        cliente.DataSource = ds;
                        cliente.DataTextField = "nombre";
                        cliente.DataValueField = "nit";
                        cliente.DataBind();
                        lista.SelectedValue = Sesion.lista;
                        lista.Items.Add(Sesion.lista);
                        fechas.Text = DateTime.Today.ToString();
                        sqlCon.Close();
                    }
                    catch (Exception)
                    {
                    }

                } 
            }
        }
        public Boolean isEmpty(String orden)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCon.Open();
                    SqlCommand comando = new SqlCommand("SELECT * FROM orden WHERE codigo='" + orden + "'", sqlCon);
                    comando.Parameters.AddWithValue("codigo", orden);
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        string a1 = reg["codigo"].ToString();
                        if (a1 == null)
                        {
                            sqlCon.Close();
                            return true;
                        }else
                        {
                            sqlCon.Close();
                            return false;
                        }
                    }
                    else
                    {
                        sqlCon.Close();
                        return true;
                    }
                }
                catch (Exception)
                {
                    return true;
                }

            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (isEmpty(codigo.Text))
            {
                string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    try
                    {
                        sqlCon.Open();
                        SqlCommand comando = new SqlCommand("SELECT producto.codigo, producto.nombre FROM producto, precios WHERE producto.codigo=precios.producto AND precios.lista='" + lista.SelectedValue.ToString() + "'", sqlCon);
                        SqlDataAdapter db = new SqlDataAdapter(comando);
                        DataSet ds = new DataSet();
                        db.Fill(ds);
                        product.DataSource = ds;
                        product.DataTextField = "nombre";
                        product.DataValueField = "codigo";
                        product.DataBind();
                        String[] fecha = DateTime.Today.ToString().Split(' ');
                        String[] nuevas = fecha[0].Split('/');
                        String nueva = nuevas[2] + "/" + nuevas[1] + "/" + nuevas[0];
                        String orden = "INSERT INTO orden (codigo, lista, empleado, cliente, costoTotal, estado, fcreacion) VALUES ('" + codigo.Text + "', '" + lista.SelectedValue.ToString() + "', '" + Sesion.usuario + "', '" + cliente.SelectedValue.ToString() + "', 0.00, 'Cargada', '" + nueva + "')";
                        SqlDataAdapter sqlDa = new SqlDataAdapter(orden, sqlCon);
                        DataTable dtbl = new DataTable();
                        sqlDa.Fill(dtbl);
                        String orden2 = "SELECT * FROM cliente WHERE nit='" + cliente.SelectedValue.ToString() + "'";
                        SqlCommand comando2 = new SqlCommand(orden2, sqlCon);
                        comando2.Parameters.AddWithValue("codigo", cliente.SelectedValue.ToString());
                        SqlDataReader reg = comando2.ExecuteReader();
                        if (reg.Read())
                        {
                            credito.Text = reg["limiteCredito"].ToString();
                            vencidas.Text = reg["ordenesVencidas"].ToString();
                        }
                        sqlCon.Close();
                        bot1.Text = "Cargada";
                        car.Enabled = true;
                        bot1.Enabled = false;
                        agregar.Enabled = true;
                        product.Enabled = true;
                        codigo.Enabled = false;
                        lista.Enabled = false;
                        cliente.Enabled = false;
                        fechas.Enabled = false;
                    }
                    catch (Exception)
                    {
                        fechas.Text = "Inválido";
                        bot1.Text = "Reintentar";
                    }
                } 
            } else {
                string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    try
                    {
                        sqlCon.Open();
                        setearOrden(codigo, cliente, fechas, lista, total);
                        codigo.Enabled = false;
                        cliente.Enabled = false;
                        fechas.Enabled = false;
                        lista.Enabled = false;
                        SqlCommand comando = new SqlCommand("SELECT codigo FROM producto WHERE lista=" + lista.Text, sqlCon);
                        SqlDataAdapter db = new SqlDataAdapter(comando);
                        DataSet ds = new DataSet();
                        db.Fill(ds);
                        product.DataSource = ds;
                        product.DataTextField = "codigo";
                        product.DataValueField = "codigo";
                        product.DataBind();
                        String orden2 = "SELECT * FROM cliente WHERE codigo=" + cliente.Text;
                        SqlCommand comando2 = new SqlCommand(orden2, sqlCon);
                        comando2.Parameters.AddWithValue("codigo", cliente.Text);
                        SqlDataReader reg = comando2.ExecuteReader();
                        if (reg.Read())
                        {
                            float credd = float.Parse(reg["limiteCredito"].ToString()) - float.Parse(total.Text);
                            credito.Text = credd.ToString();
                            vencidas.Text = reg["ordenesVencidas"].ToString();
                        }
                        sqlCon.Close();
                        bot1.Text = "Cargada";
                        bot1.Enabled = false;
                        agregar.Enabled = true;
                        product.Enabled = true;
                        codigo.Enabled = false;
                        lista.Enabled = false;
                        cliente.Enabled = false;
                        fechas.Enabled = false;
                    }
                    catch (Exception)
                    {
                        fechas.Text = "Inválido";
                        bot1.Text = "Reintentar";
                    }
                }
            }
        }
        public void  setearOrden(TextBox codigo, DropDownList cliente, TextBox fechas, DropDownList lista, Label total)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCon.Open();
                    SqlCommand comando = new SqlCommand("SELECT * FROM orden WHERE codigo='" + codigo.Text + "'", sqlCon);
                    comando.Parameters.AddWithValue("codigo", codigo.Text);
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        cliente.Text = reg["cliente"].ToString();
                        fechas.Text = reg["fcreacion"].ToString();
                        lista.Text = reg["lista"].ToString();
                        total.Text = reg["costoTotal"].ToString();
                    }
                    sqlCon.Close();
                }
                catch (Exception)
                {
                }

            }
        }
        protected void agregar_Click(object sender, EventArgs e)
        {
            try
            {
                int canti;
                float precio = float.Parse(price.Text);
                float actual = float.Parse(total.Text);
                int cantidad = int.Parse(cant.Text);
                float tot = (precio * cantidad) + actual;
                float cred = float.Parse(credito.Text) - (precio * cantidad);

                if (cred >= 0)
                {
                    if ((cantidad > 0) && (cantidad <= int.Parse(existencias.Text.ToString())))
                    {
                        

                        string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

                        using (SqlConnection sqlCon = new SqlConnection(connectionString))
                        {
                            try
                            {
                                eliminar.Enabled = true;
                                credito.Text = cred.ToString();
                                total.Text = tot.ToString();
                                generar.Enabled = true;
                                sqlCon.Open();
                                float subtotal = float.Parse(cant.Text) * float.Parse(price.Text);
                                String orden = "INSERT INTO carrito (producto, subTotal, codigo, precio, cantidad, orden) VALUES ('" + product.SelectedValue.ToString() + "', " + subtotal + ", '" + codigo.Text + "." + product.SelectedValue.ToString() + "', " + price.Text + ", " + cant.Text + ", '" + codigo.Text + "')";
                                SqlDataAdapter sqlDa = new SqlDataAdapter(orden, sqlCon);
                                DataTable dtbl = new DataTable();
                                sqlDa.Fill(dtbl);
                                ejecutar1("UPDATE orden SET costoTotal=" + total.Text + " WHERE codigo='" + codigo.Text + "'");
                                sqlCon.Close();
                                
                            }
                            catch (Exception)
                            {
                                String orden = "SELECT * FROM carrito WHERE codigo='" + codigo.Text + "." + product.SelectedValue.ToString() + "'";
                                SqlCommand comando = new SqlCommand(orden, sqlCon);
                                comando.Parameters.AddWithValue("codigo", product.SelectedValue.ToString());
                                SqlDataReader reg = comando.ExecuteReader();
                                if (reg.Read())
                                {
                                    canti = int.Parse(reg["cantidad"].ToString());
                                    int nuevo = canti + int.Parse(cant.Text);
                                    float subtot = nuevo * float.Parse(price.Text);
                                    total.Text = tot.ToString();
                                    String comand = "UPDATE carrito SET cantidad=" + nuevo + ", subTotal=" + subtot + " WHERE codigo='" + codigo.Text + "." + product.SelectedValue.ToString() + "'";
                                    ejecutar1(comand);
                                    ejecutar1("UPDATE orden SET costoTotal=" + total.Text + " WHERE codigo='" + codigo.Text + "'");
                                    credito.Text = cred.ToString();
                                }
                                sqlCon.Close();
                            }
                        }
                    }
                    else
                    {
                        agregar.Text = "Reintentar";
                        cant.Text = "Inválido";
                    }
                }
                else
                {
                    agregar.Text = "Reintentar";
                    cant.Text = "Inválido";
                }
            }
            catch (Exception)
            {
                cant.Text = "Inválido";
                agregar.Text = "Reintentar";
            }
        }
        public int dia(string cood)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                try
                {
                    String orden = "SELECT * FROM orden WHERE codigo='" + cood + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    comando.Parameters.AddWithValue("codigo", cood);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        string [] inicio = reg["fcreacion"].ToString().Split(' ');
                        string[] fechaa = inicio[0].Split('/');
                        string fecha = fechaa[2] + "/" + fechaa[1] + "/" + fechaa[0];
                        DateTime beg = Convert.ToDateTime(fecha);
                        TimeSpan periodo = DateTime.Today - beg;
                        sqlCon.Close();
                        return periodo.Days;
                    }
                    else
                    {
                        sqlCon.Close();
                        return 0;
                    }

                }
                catch (Exception)
                {
                    return 0;
                }

            }
        }
        protected void generar_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCon.Open();
                    string [] fechaa = DateTime.Today.ToString().Split(' ');
                    string[] fechab = fechaa[0].Split('/');
                    string nueva = fechab[2] + "/" + fechab[1] + "/" + fechab[0];
                    SqlCommand comando = new SqlCommand("UPDATE orden SET estado='Cerrada', fcierre='" + nueva + "', diasC=" + dia(codigo.Text) + " WHERE codigo='" + codigo.Text + "'", sqlCon);
                    comando.ExecuteNonQuery();
                    comando = new SqlCommand("UPDATE orden SET costoTotal=" + total.Text + " WHERE codigo='" + codigo.Text + "'", sqlCon);
                    comando.ExecuteNonQuery();
                    comando = new SqlCommand("UPDATE cliente SET limiteCredito=" + credito.Text + " WHERE nit='" + cliente.SelectedValue.ToString() + "'", sqlCon);
                    comando.ExecuteNonQuery();
                    sqlCon.Close();
                    generar.Text = "Orden Cerrada";
                    car.Enabled = false;
                    generar.Enabled = false;
                    agregar.Enabled = false;
                    product.Enabled = false;
                    eliminar.Enabled = false;
                    cant.Enabled = false;
                }
                catch (Exception)
                {
                }

            }
        }

        protected void product_SelectedIndexChanged(object sender, EventArgs e)
        {
            agregar.Text = "Agregar";
            agregar.Enabled = true;
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {

                try
                {
                    String orden = "SELECT categoria.nombre, precios.precio, producto.existencias FROM categoria, precios, producto WHERE producto.categoria=categoria.codigo AND precios.producto=producto.codigo AND precios.lista='" + Sesion.lista + "' AND producto.codigo='" + product.SelectedValue.ToString() + "'";
                    SqlCommand comando = new SqlCommand(orden, sqlCon);
                    sqlCon.Open();
                    SqlDataReader reg = comando.ExecuteReader();
                    if (reg.Read())
                    {
                        nombre.Text = reg["nombre"].ToString();
                        price.Text = reg["precio"].ToString();
                        existencias.Text = reg["existencias"].ToString();
                    }

                    sqlCon.Close();
                }
                catch (Exception)
                {
                }

            }
        }

        public void ejecutar1(String cmd)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCon.Open();
                    SqlCommand comando = new SqlCommand(cmd, sqlCon);
                    comando.ExecuteNonQuery();
                    sqlCon.Close();
                }
                catch (Exception)
                {
                }
            }
        }
        protected void eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                int canti;
                float precio = float.Parse(price.Text);
                float actual = float.Parse(total.Text);
                int cantidad = int.Parse(cant.Text);
                float tot = actual - (precio * cantidad);
                float cred = float.Parse(credito.Text) + (precio * cantidad);
                if (tot >= 0)
                {
                    if ((cantidad > 0) && (cantidad <= int.Parse(existencias.Text.ToString())))
                    {
                        string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

                        using (SqlConnection sqlCon = new SqlConnection(connectionString))
                        {
                            try
                            {
                                String orden = "SELECT * FROM carrito WHERE codigo='" + codigo.Text + "." + product.SelectedValue.ToString() + "'";
                                SqlCommand comando = new SqlCommand(orden, sqlCon);
                                comando.Parameters.AddWithValue("codigo", product.SelectedValue.ToString());
                                sqlCon.Open();
                                SqlDataReader reg = comando.ExecuteReader();
                                if (reg.Read())
                                {
                                    canti = int.Parse(reg["cantidad"].ToString());
                                    float subtotal = float.Parse(reg["subTotal"].ToString());
                                    subtotal -= (int.Parse(cant.Text) * float.Parse(price.Text));
                                    if (int.Parse(cant.Text) <= canti)
                                    {
                                        int nuevo = canti - int.Parse(cant.Text);
                                        total.Text = tot.ToString();
                                        String comand = "UPDATE carrito SET cantidad=" + nuevo + ", subTotal=" + subtotal + " WHERE codigo='" + codigo.Text + "." + product.SelectedValue.ToString() + "'";
                                        ejecutar1(comand);
                                        ejecutar1("UPDATE orden SET costoTotal=" + total.Text + " WHERE codigo='" + codigo.Text + "'");
                                        credito.Text = cred.ToString();
                                    }
                                }
                                sqlCon.Close();
                            }
                            catch (Exception)
                            {
                                
                            }
                        }
                    }
                    else
                    {
                        eliminar.Text = "Reintentar";
                        cant.Text = "Inválido";
                    }
                }
                else
                {
                    eliminar.Text = "Reintentar";
                    cant.Text = "Inválido";
                }
            }
            catch (Exception)
            {
                cant.Text = "Inválido";
                eliminar.Text = "Reintentar";
            }
        }
    }
}