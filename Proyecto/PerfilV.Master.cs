﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class PerfilV : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Sesion.usuario != null)
            {
                string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    try
                    {
                        String orden = "SELECT * FROM empleado WHERE nit='" + Sesion.usuario + "'";
                        SqlCommand comando = new SqlCommand(orden, sqlCon);
                        comando.Parameters.AddWithValue("nit", Sesion.usuario);
                        sqlCon.Open();
                        SqlDataReader reg = comando.ExecuteReader();
                        if (reg.Read())
                        {
                            nombres.Text = reg["nombres"].ToString();
                            sqlCon.Close();
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else
            {
                Response.Redirect("/Sesion.aspx");
            }
        }

        protected void sesion_Click(object sender, EventArgs e)
        {
            Sesion.usuario = null;
            Response.Redirect("/Sesion.aspx");
        }
    }
}