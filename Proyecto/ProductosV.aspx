﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/PerfilV.Master" AutoEventWireup="true" CodeBehind="ProductosV.aspx.cs" Inherits="Proyecto.ProductosV" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Perfiles" runat="server">
    <style>
        article {
            height:70%;
            width:77.5%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:22%;
            left:4%;
            overflow:auto;
        }
        .busqueda {
            height:7%;
            width:65%;
            color:white;
            text-align:center;
            font-size:170%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:7.5%;
            left:4%;
        }
        .boton {
            height:8%;
            width:9%;
            color:white;
            font-size:180%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:7.5%;
            left:72.7%;
            transition-duration:0.3s;
        }
        .boton:hover {
            height:8%;
            width:9%;
            color:black;
            font-size:180%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:7.5%;
            left:72.7%;
            transition-duration:0.3s;
        }
        #contenido {
            height:90%;
            width:94%;
            font-size:130%;
            position:relative;
            top:1%;
            left:3%;
            background-color:black;
        }
        #meta {
            height:8.3%;
            width:36%;
            text-align:center;
            font-size:120%;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:-0.3%;
            left:63.8%;
            background-color:black;
        }
        h2{
            margin-left:-2%;
            margin-top:0%;
        }
        table {
            margin-left:1%;
            margin-top:-1%;
            font-size:70%;
            background-color:black;
        }
        td {
            border-left:45px solid black;
            border-right:45px solid black;
            border-bottom:45px solid black;
        }
        .producto {
            padding:35% 17%;
            background-color:rgb(25,25,25);
            border-radius:10px;
            border:1px solid white;
        }
        .producto:active {
            padding:21% 25%;
            position:fixed;
            top:5%;
            left:32.4%;
            background-color:rgb(25,25,25);
            border-radius:10px;
            border:1px solid white;
        }
    </style>
        <asp:TextBox ID="TextBox1" CssClass="busqueda" runat="server" placeholder="Ingrese Código del Producto"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" CssClass="boton" Text="Buscar"/>
        <article>
            <div id="contenido">
                <h2>Productos:</h2>
                <table border="0">
                    <tr>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                    </tr><tr>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                    </tr><tr>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                        <td><div class="producto">Imagen del producto</div><br />Nombre: (Nombre del Producto)<br />Precio: (Precio del Producto)<br />Existencias: (No. de Existencias)</td>
                    </tr>
                </table>
            </div>
        </article>
</asp:Content>
