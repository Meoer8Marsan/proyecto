﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/PerfilV.Master" AutoEventWireup="true" CodeBehind="OrdenPagoV.aspx.cs" Inherits="Proyecto.OrdenPagoV" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Perfiles" runat="server">
    <style>
        article {
            height:60%;
            width:43%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:12%;
            left:2%;
        }
        #contenido {
            height:90%;
            width:94%;
            font-size:130%;
            position:relative;
            font-size:120%;
            top:0%;
            left:3%;
            background-color:black;
        }
        .t1 {
            height:100%;
            width:100%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            color:white;
            font-family:"Times New Roman", Arial;
            font-size:80%;
            text-align:center;
        }
        .boton {
            height:15%;
            width:25%;
            color:white;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:90%;
            left:37.5%;
            transition-duration:0.3s;
        }
        .boton:hover {
            height:15%;
            width:25%;
            color:black;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:90%;
            left:37.5%;
            transition-duration:0.3s;
        }
        #article2 {
            height:60%;
            width:35.5%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:12%;
            left:47.5%;
        }
        .asc {
            height:100%;
            width:60%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            color:white;
            font-family:"Times New Roman", Arial;
            font-size:80%;
            text-align:center;
        }
        .boton2 {
            height:10%;
            width:25%;
            color:white;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:85%;
            left:20%;
            transition-duration:0.3s;
        }
        .boton2:hover {
            height:10%;
            width:25%;
            color:black;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:85%;
            left:20%;
            transition-duration:0.3s;
        }
        .eli {
            height:10%;
            width:25%;
            color:white;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:85%;
            left:55%;
            transition-duration:0.3s;
        }
        .eli:hover {
            height:10%;
            width:25%;
            color:black;
            font-size:120%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:85%;
            left:55%;
            transition-duration:0.3s;
        }
        .boton3 {
            height:10%;
            width:25%;
            color:white;
            font-size:150%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:78%;
            left:30%;
            transition-duration:0.3s;
        }
        .boton3:hover {
            height:10%;
            width:25%;
            color:black;
            font-size:150%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:78%;
            left:30%;
            transition-duration:0.3s;
        }
        h3{
            position:absolute;
            top:16%;
            left:16%;
        }
        .prod {
            height:7%;
            width:13%;
            position:absolute;
            top:28.5%;
            left:86%;
            border-radius:10px;
            border:2px solid white;
            background-color:black;
            font-size:100%;
            color:white;
            font-family:"Times New Roman", Arial;
            cursor:pointer;
            transition-duration:0.3s;
        }
        .prod:hover {
            height:7%;
            width:13%;
            position:absolute;
            top:28.5%;
            left:86%;
            border-radius:10px;
            border:2px solid white;
            background-color:white;
            font-size:100%;
            color:black;
            font-family:"Times New Roman", Arial;
            cursor:pointer;
            transition-duration:0.3s;
        }
    </style>
        <article>
            <div id="contenido">
                <center><br />
                <h1>Orden de Pago Nueva</h1><br />
                <table border="0">
                    <tr>
                        <td>Ingrese el Código de la Orden <br />&nbsp;</td><td><asp:TextBox ID="codigo" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Ingrese Cliente <br />&nbsp;</td><td><asp:DropDownList CssClass="t1" ID="cliente" runat="server" Enabled="true"></asp:DropDownList><br />&nbsp;</td>
                    </tr><tr>
                        <td>Fecha de Creación <br />&nbsp;</td><td><asp:TextBox ID="fechas" CssClass="t1" runat="server" placeholder="DD/MM/YYYY" Enabled="false"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Ingrese lista de Precios Asociada &nbsp;&nbsp;<br />&nbsp;</td><td><asp:DropDownList CssClass="t1" ID="lista" runat="server" Enabled="true"></asp:DropDownList><br />&nbsp;</td>
                    </tr>
                </table>
                    <asp:Button ID="bot1" runat="server" CssClass="boton" Text="Cargar" OnClick="Button1_Click" />
                </center>
            </div>
        </article>
        <div id="article2">
            <center><br />
                <h1>Agregar productos a la orden</h1><br />
                <h3>Crédito: <asp:Label ID="credito" runat="server"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Órdenes Vencidas: <asp:Label ID="vencidas" runat="server"></asp:Label></h3>
                <table border="0"><tr>
                        <td>Código de producto <br />&nbsp;</td><td><asp:DropDownList CssClass="t1" ID="product" runat="server" Enabled="false"></asp:DropDownList><br />&nbsp;</td>
                    </tr><tr>
                        <td>Categoría <br />&nbsp;</td><td><asp:TextBox Enabled="false" ID="nombre" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Precio unitario &nbsp;&nbsp;<br />&nbsp;</td><td><asp:TextBox Enabled="false" ID="price" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Existencias &nbsp;&nbsp;<br />&nbsp;</td><td><asp:TextBox Enabled="false" ID="existencias" CssClass="t1" runat="server"></asp:TextBox><br />&nbsp;</td>
                    </tr><tr>
                        <td>Total: $<asp:Label ID="total" runat="server" Text="0.00"></asp:Label><br />&nbsp;</td><td><asp:TextBox ID="cant" CssClass="t1" runat="server" placeholder="Cantidad"></asp:TextBox> &nbsp;&nbsp;<br />&nbsp;</td> 
                    </tr>
                </table>
                    <asp:Button ID="car" Enabled="false" runat="server" CssClass="prod" Text="Cargar" OnClick="product_SelectedIndexChanged" />
                    <asp:Button ID="agregar" Enabled="false" runat="server" CssClass="boton2" Text="Agregar" OnClick="agregar_Click" />
                    <asp:Button ID="eliminar" Enabled="false" runat="server" CssClass="eli" Text="Eliminar" OnClick="eliminar_Click" />
                </center>
        </div>
                    <asp:Button ID="generar" Enabled="false" runat="server" CssClass="boton3" Text="Cerrar Orden" OnClick="generar_Click" />
</asp:Content>
