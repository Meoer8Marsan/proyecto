﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class Registro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=DESKTOP-TFFV004; Initial Catalog = Proyecto; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                if (gerente.Checked == true)
                {
                    try
                    {
                        sqlCon.Open();
                        String[] fecha = nacimimeto.Text.Split('/');
                        String nueva = fecha[2] + "/" + fecha[1] + "/" + fecha[0];
                        String orden = "INSERT INTO empleado (nit, nombres, apellidos, fechaNacimiento, direccion, numCasa, numCelular, email, contrasena, tipo) VALUES ('" + nit.Text + "' , '" + nombres.Text + "' , '" + apellidos.Text + "' , '" + nueva + "' , '" + direccion.Text + "' , '" + telCasa.Text + "' , '" + celular.Text + "' , '" + email.Text + "' , '" + contra.Text + "' , 'Gerente')";
                        SqlDataAdapter sqlDa = new SqlDataAdapter(orden, sqlCon);
                        DataTable dtbl = new DataTable();
                        sqlDa.Fill(dtbl);
                        sqlCon.Close();
                        conf.Text = "El registro del gerente fue completado con éxito";
                    }
                    catch (Exception)
                    {
                        conf.Text = "Error al ingresar, por favor revise sus datos e intente de nuevo";
                    } 
                } else if (supervisor.Checked == true)
                {
                    try
                    {
                        sqlCon.Open();
                        String[] fecha = nacimimeto.Text.Split('/');
                        String nueva = fecha[2] + "/" + fecha[1] + "/" + fecha[0];
                        SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO empleado (nit, nombres, apellidos, fechaNacimiento, direccion, numCasa, numCelular, email, contrasena, tipo) VALUES ('" + nit.Text + "' , '" + nombres.Text + "' , '" + apellidos.Text + "' , '" + nueva + "' , '" + direccion.Text + "' , '" + telCasa.Text + "' , '" + celular.Text + "' , '" + email.Text + "' , '" + contra.Text + "' , 'Supervisor')", sqlCon);
                        DataTable dtbl = new DataTable();
                        sqlDa.Fill(dtbl);
                        sqlCon.Close();
                        conf.Text = "El registro del gerente fue completado con éxito";
                    }
                    catch (Exception)
                    {
                        conf.Text = "Error al ingresar, por favor revise sus datos e intente de nuevo";
                    }
                } else if (vendedor.Checked == true)
                {
                    try
                    {
                        sqlCon.Open();
                        String[] fecha = nacimimeto.Text.Split('/');
                        String nueva = fecha[2] + "/" + fecha[1] + "/" + fecha[0];
                        SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO empleado (nit, nombres, apellidos, fechaNacimiento, direccion, numCasa, numCelular, email, contrasena, tipo) VALUES ('" + nit.Text + "' , '" + nombres.Text + "' , '" + apellidos.Text + "' , '" + nueva + "' , '" + direccion.Text + "' , '" + telCasa.Text + "' , '" + celular.Text + "' , '" + email.Text + "' , '" + contra.Text + "' , 'Vendedor')", sqlCon);
                        DataTable dtbl = new DataTable();
                        sqlDa.Fill(dtbl);
                        sqlCon.Close();
                        conf.Text = "El registro del supervisor fue completado con éxito";
                    }
                    catch (Exception)
                    {
                        conf.Text = "Error al ingresar, por favor revise sus datos e intente de nuevo";
                    }
                } 
            }
        }
    }
}