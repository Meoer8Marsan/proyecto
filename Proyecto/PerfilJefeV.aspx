﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/PerfilV.Master" AutoEventWireup="true" CodeBehind="PerfilJefeV.aspx.cs" Inherits="Proyecto.PerfilJefeV" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Perfiles" runat="server">
     <style>
        article {
            height:86%;
            width:77.5%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:7%;
            left:4%;
        }
        #contenido {
            height:90%;
            width:94%;
            font-size:130%;
            position:relative;
            top:2%;
            left:3%;
            background-color:black;
        }
        #meta {
            height:8.3%;
            width:36%;
            text-align:center;
            font-size:120%;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:-0.3%;
            left:63.8%;
            background-color:black;
        }
        h1 {
            font-size:170%;
        }
    </style>
        <article>
            <div id="contenido">
                <h1>Perfil de Jefe</h1>
                <h2>Datos Personales</h2>
                Nombre: <asp:Label ID="nombre" CssClass="label" runat="server"></asp:Label><p></p>
                Fecha de Nacimiento: <asp:Label ID="nacimiento" CssClass="label" runat="server"></asp:Label><p></p>
                Número de Celular: <asp:Label ID="numCelular" CssClass="label" runat="server"></asp:Label><br /><br />
                <h2>Datos de Empleado</h2>
                NIT: <asp:Label ID="nit" CssClass="label" runat="server"></asp:Label><p></p>
                Correo electrónico: <asp:Label ID="email" CssClass="label" runat="server"></asp:Label><p></p>
            </div>
        </article>
</asp:Content>
