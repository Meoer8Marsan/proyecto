﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/PerfilV.Master" AutoEventWireup="true" CodeBehind="ListaPreciosV.aspx.cs" Inherits="Proyecto.ListaPreciosV" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Perfiles" runat="server">
    <style>
        article {
            height:70%;
            width:77.5%;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:22%;
            left:4%;
            overflow:auto;
        }
        .busqueda {
            height:7%;
            width:65%;
            color:white;
            text-align:center;
            font-size:170%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:7.5%;
            left:4%;
        }
        .boton {
            height:8%;
            width:9%;
            color:white;
            font-size:180%;
            font-family:"Times New Roman", Arial;
            background-color:black;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:7.5%;
            left:72.7%;
            transition-duration:0.3s;
        }
        .boton:hover {
            height:8%;
            width:9%;
            color:black;
            font-size:180%;
            font-family:"Times New Roman", Arial;
            background-color:white;
            border:2px solid white;
            border-radius:10px;
            cursor:pointer;
            position:absolute;
            top:7.5%;
            left:72.7%;
            transition-duration:0.3s;
        }
        #contenido {
            height:90%;
            width:94%;
            font-size:130%;
            position:relative;
            top:1%;
            left:3%;
            background-color:black;
        }
        #meta {
            height:8.3%;
            width:36%;
            text-align:center;
            font-size:120%;
            border:2px solid white;
            border-radius:10px;
            position:absolute;
            top:-0.3%;
            left:63.8%;
            background-color:black;
        }
        a{
            text-decoration:none;
        }
        table {
            margin-left:7%;
        }
    </style>
        <asp:TextBox ID="TextBox1" CssClass="busqueda" runat="server" placeholder="Ingrese Código de la Lista a Revisar"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" CssClass="boton" Text="Buscar"/>
        <article>
            <div id="contenido">
                <h2>Lista (Código de Lista)</h2>
                <table border="0">
                    <tr>
                        <td>&nbsp;1  &nbsp; &nbsp; &nbsp; <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto)  &nbsp;&nbsp;&nbsp; &nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto)  &nbsp; &nbsp; &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr><tr>
                        <td>&nbsp;2 <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr><tr>
                        <td>&nbsp;3 <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr><tr>
                        <td>&nbsp;4 <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr><tr>
                        <td>&nbsp;5 <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr><tr>
                        <td>&nbsp;6 <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr><tr>
                        <td>&nbsp;7 <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr><tr>
                        <td>&nbsp;8 <br /> &nbsp; <br /></td><td>Nombre: (Nombre del producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td>Precio: (Precio del Producto) &nbsp;&nbsp; <br />&nbsp;&nbsp;</td><td><a href=""><font color="white">Ver Producto</font></a> <br />&nbsp;&nbsp;</td>
                    </tr>
                </table>
            </div>
        </article>
</asp:Content>
