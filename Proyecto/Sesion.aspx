﻿<%@ Page Title="DIPROMA S.A." Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Sesion.aspx.cs" Inherits="Proyecto.Sesion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <style>
        #inicio{
            width:30%;
            height:50%;
            position:absolute;
            top:20%;
            left:35%;
            background-color:rgb(25,25,25);
            border:5px double white;
            color:white;
            border-radius:20px;
        }
        #titulo {
            width:100%;
            height:10%;
            color:white;
            font-size:250%;
            text-align:center;
        }
        h5 {
            width:100%;
            height:10%;
            color:white;
            font-size:120%;
            text-align:center;
        }
        #box{
            width:72%;
            height:50%;
            color:white;
            font-size:120%;
            position:relative;
            top:2%;
            left:14%;
            background-color:rgb(25,25,25);
            color:white;
        }
        #t1 {
            float:right;
        }
        #t2 {
            float:right;
        }
        #b1 {
            background-color:rgb(25,25,25);
        }
        .active {
            width:36%;
            height:30%;
            background-color:rgb(25,25,25);
            border:2px solid white;
            color:white;
            position:relative;
            top:2%;
            left:32%;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .active:hover {
            width:36%;
            height:30%;
            background-color:white;
            border:2px solid rgb(25,25,25);
            color:black;
            position:relative;
            top:2%;
            left:32%;
            border-radius:10px;
            transition-duration:0.3s;
        }
        .t {
            background-color:rgb(25,25,25);
            border:2px solid white;
            border-radius:10px;
            color:white;
            text-align:center;
        }
        #registro {
            width:20%;
            height:7%;
            font-size:110%;
            background-color:black;
            position:absolute;
            top:73%;
            left:40%;
            color:white;
            text-align:center;
        }
        #ref {
            text-decoration:none;
            text-decoration-color:white;
            color:white;
        }
        #foot {
            width:85.5%;
            height:9%;
            background-color:black;
            position:absolute;
            top:86%;
            left:6.5%;
        }
        a {
            color:white;
        }
        .label {
            width:100%;
            height:15%;
            margin-left:0%;
            margin-top:4%;
            background-color:rgb(25,25,25);
            position:absolute;
            text-align:center;
            font-size:80%;
        }
    </style>
    <div id="inicio">
        <h2 id="titulo">Iniciar Sesión</h2><br />
        <h5>Ingrese sus datos por favor</h5>
        <div id="box">
            Usuario<div id="t1"><asp:TextBox ID="nit" CssClass="t" runat="server" placeholder="Ingrese NIT"></asp:TextBox></div>
            <br />
            <br />
            Contraseña <div id="t2"><asp:TextBox TextMode="Password" ID="contra" CssClass="t" runat="server"></asp:TextBox></div>
            <br />
            <br />
            <asp:Button ID="Button" runat="server" CssClass="active" Text="Ingresar" OnClick ="Button1_Click" /><br />
            <asp:Label ID="conf" runat="server" CssClass="label" ></asp:Label>
        </div>
    </div>
    <br />
    <br />
    <div id="registro">
        Si aun no tienes una cuenta puedes<br />registrarte haciendo click <a id="ref" runat="server" href="~/Registro">aquí.</a>
    </div>
</asp:Content>
